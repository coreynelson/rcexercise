﻿using System;
using System.Diagnostics.Contracts;

namespace RC.CodingChallenge.FaultPatternLib
{
    /// <summary>
    /// Base class for States for the IStateMachine
    /// </summary>
    public abstract class State
    {
        /// <param name="machine">The IStateMachine this object is resposible for updating when
        /// stage changes.</param>
        public State(IStateMachine machine)
        {
            Contract.Requires(machine != null);
            Contract.Ensures(Machine == machine);

            Machine = machine;
        }

        /// <summary>
        /// The IStateMachine this object is resposible for updating when stage changes.
        /// </summary>
        protected IStateMachine Machine { get; private set; }

        /// <summary>
        /// Called to update the State of the IStateMachine based on the current State and the
        /// given StageUpdate
        /// </summary>
        /// <param name="update">The new date and stage of the HVAC unit.</param>
        /// <returns>Returns true if a fault sequence has been detected, otherwise false</returns>
        public abstract bool UpdateMachineState(StageUpdate update);
    }

    /// <summary>
    /// Represents the state of the HVAC unit whenever it's not in one of the other states.
    /// </summary>
    public class DefaultState : State
    {
        public DefaultState(IStateMachine machine) : base(machine)
        {
        }

        public override bool UpdateMachineState(StageUpdate update)
        {
            Contract.Assert(update != null);

            if (update.Stage == Stage.Three)
            {
                // We begin waiting in stage 3.
                Machine.State = new Wait3State(Machine, update.Date);
            }
            // else stay in Default

            return false;
        }
    }

    /// <summary>
    /// Represent the state of the HVAC unit when it has been in stage 3 for less than five minutes,
    /// but not yet cycling between 2 and 3.
    /// </summary>
    public class Wait3State : State
    {
        // Used to keep track of how long we've been in stage 3.
        private DateTimeOffset _startDate;

        /// <param name="startDate">The date time the HVAC unit first switched into stage 3.</param>
        public Wait3State(IStateMachine machine, DateTimeOffset startDate) : base(machine)
        {
            _startDate = startDate;
        }

        public override bool UpdateMachineState(StageUpdate update)
        {
            Contract.Assert(update != null);

            switch (update.Stage)
            {
                case Stage.Three:
                    {
                        // This is just a "heartbeat" update, stay in Wait3State.
                        break;
                    }
                case Stage.Two:
                    {
                        TimeSpan waitTime = update.Date.Subtract(_startDate);
                        if (waitTime > TimeSpan.FromMinutes(5))
                        {
                            // We've been in stage 3 for more than 5 minutes and have begun cycling
                            // between stage 2 and 3.
                            Machine.State = new Cycle23State(Machine);
                        }
                        else
                        {
                            // We haven't spent 5 minutes in stage 3 so go back to DefaultState.
                            Machine.State = new DefaultState(Machine);
                        }
                        break;
                    }
                default:
                    {
                        Machine.State = new DefaultState(Machine);
                        break;
                    }
            }

            return false;
        }
    }

    /// <summary>
    /// Represents the state of the HVAC unit after is spent 5 minutes in stage 3 and is now
    /// cycling between 2 and 3.
    /// </summary>
    public class Cycle23State : State
    {
        public Cycle23State(IStateMachine machine) : base(machine)
        {
        }

        public override bool UpdateMachineState(StageUpdate update)
        {
            Contract.Assert(update != null);

            switch (update.Stage)
            {
                case Stage.Three:
                case Stage.Two:
                    {
                        // This is either a 2/3 cycle or just a "heartbeat" update, stay in
                        // Cycle23State.
                        break;
                    }
                case Stage.Zero:
                    {
                        // We found a fault sequence!
                        Machine.State = new DefaultState(Machine);
                        return true;
                    }
                default:
                    {
                        Machine.State = new DefaultState(Machine);
                        break;
                    }
            }

            return false;
        }
    }
}