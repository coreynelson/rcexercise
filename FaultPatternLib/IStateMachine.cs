﻿namespace RC.CodingChallenge.FaultPatternLib
{
    /// <summary>
    /// A StateMachine keeps track of it's State. The States themselve update the State of the StateMachine.
    /// </summary>
    public interface IStateMachine
    {
        State State { get; set; }

        bool UpdateStage(StageUpdate stageUpdate);
    }
}