﻿using System;
using System.Diagnostics.Contracts;

namespace RC.CodingChallenge.FaultPatternLib
{
    /// <summary>
    /// Represents the date and stage from a event log stream.
    /// </summary>
    public class StageUpdate
    {
        public StageUpdate(DateTimeOffset date, Stage stage)
        {
            Contract.Ensures(Date == date);
            Contract.Ensures(Stage == stage);

            Date = date;
            Stage = stage;
        }

        public DateTimeOffset Date { get; private set; }

        public Stage Stage { get; private set; }
    }
}