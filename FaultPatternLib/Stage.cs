﻿namespace RC.CodingChallenge.FaultPatternLib
{
    /// <summary>
    /// Used to keep track the stage the HVAC unit is in. I wish I could give these more descriptive
    /// names but I lack context in this exercise.
    /// </summary>
    public enum Stage
    {
        Zero = 0,
        One = 1,
        Two = 2,
        Three = 3,
    }
}