﻿using System;
using System.Diagnostics.Contracts;
using System.IO;

namespace RC.CodingChallenge.FaultPatternLib
{
    /// <summary>
    /// A service that identifies and counts events which are associated with a specific sequence of operations.
    ///
    /// The input logs are comprised of lines of text that indicate the time of a recording, and the
    /// value recorded
    ///
    ///      1998-03-07 06:25:32	2
    ///      1998-03-07 09:15:55	3
    ///      1998-03-07 12:00:02	3
    ///      1998-03-07 14:28:27	0
    ///
    /// The columns (1) date+time in ISO-8601 format, (2) value indicating HVAC unit stage by tabs.
    /// </summary>
    public class HvacFaultCounter : IEventCounter
    {
        private readonly string _invalidDataExceptionMessage = "eventLog lines must be in the format '1998-03-07 06:25:32	2'. Invalid line: ";

        private readonly StringCounter _counter = new StringCounter();

        public int GetEventCount(string deviceID)
        {
            return _counter.GetCount(deviceID);
        }

        public void ParseEvents(string deviceID, StreamReader eventLog)
        {
            Contract.Assert(deviceID != null);
            Contract.Assert(eventLog != null);

            Hvac hvac = new Hvac(deviceID);

            string line;
            while ((line = eventLog.ReadLine()) != null)
            {
                // ignore blank lines
                if (string.IsNullOrWhiteSpace(line)) continue;

                StageUpdate update = ParseEvent(line);

                bool fault = hvac.UpdateStage(update);

                if (fault)
                {
                    _counter.IncrementCount(deviceID);
                }
            }
        }

        /// <summary>
        /// Parses a single line of the event log and converts it to a StageUpdate. Throws a 
        /// InvalidDataException if the data is not formated correctly.
        /// </summary>
        private StageUpdate ParseEvent(string line)
        {
            Contract.Requires(!IsNullOrWhiteSpace(line));
            Contract.Ensures(Contract.Result<StageUpdate>() != null);

            string[] arr = line.Split('\t');
            if (arr.Length != 2)
            {
                throw new InvalidDataException(_invalidDataExceptionMessage + line);
            }

            // Parse the date.
            DateTimeOffset date;
            if (!DateTimeOffset.TryParse(arr[0], out date))
            {
                throw new InvalidDataException(_invalidDataExceptionMessage + line);
            }

            // Parse the stage
            Stage stage;
            if (!Stage.TryParse(arr[1], out stage))
            {
                throw new InvalidDataException(_invalidDataExceptionMessage + line);
            }

            return new StageUpdate(date, stage);
        }

        // Code Contract complains if I call string.IsNullOrWhiteSpace directly because it doesn't
        // know if the method id Pure. So I wrapped it in this Pure method.
        [Pure]
        private bool IsNullOrWhiteSpace(string s)
        {
            return string.IsNullOrWhiteSpace(s);
        }
    }
}