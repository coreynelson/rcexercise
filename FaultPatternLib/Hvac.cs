﻿using System.Diagnostics.Contracts;

namespace RC.CodingChallenge.FaultPatternLib
{
    /// <summary>
    /// Represents an HVAC unit. It stores the units deviceId and State.
    /// </summary>
    public class Hvac : IStateMachine
    {
        public Hvac(string deviceId)
        {
            Contract.Requires(deviceId != null);
            Contract.Ensures(DeviceId == deviceId);
            Contract.Ensures(State is DefaultState);

            DeviceId = deviceId;
            State = new DefaultState(this);
        }

        public string DeviceId { get; private set; }

        public State State { get; set; }
 
        public bool UpdateStage(StageUpdate stageUpdate)
        {
            Contract.Assert(stageUpdate != null);

            return State.UpdateMachineState(stageUpdate);
        }
    }
}