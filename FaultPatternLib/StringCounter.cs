﻿using System.Collections.Generic;
using System.Diagnostics.Contracts;

namespace RC.CodingChallenge.FaultPatternLib
{
    /// <summary>
    /// A thread-safe simple Dictionary to keep track of counts for string keys.
    /// </summary>
    public class StringCounter
    {
        private readonly Dictionary<string, int> _dict = new Dictionary<string, int>();
        private readonly object _syncLock = new object();

        /// <summary>
        /// Get the count associated with a given key.
        /// </summary>
        /// <param name="key">The key that you want the count for. Cannot be null.</param>
        /// <returns>
        /// The count associated with the key. If the key is not in the dictionary (hasn't been
        /// incremented yet), return 0.
        /// </returns>
        public int GetCount(string key)
        {
            Contract.Requires(key != null);

            int count;
            return _dict.TryGetValue(key, out count) ?
                count :
                0;
        }

        /// <summary>
        /// Increment the count associated with a given key. If the key doesn't exist in the
        /// dictionary (hasn't been previously incrmented yet), it will be inserted with a count of 1.
        /// </summary>
        /// <param name="key">The key to increment the count for. Cannot be null.</param>
        public void IncrementCount(string key)
        {
            Contract.Requires(key != null);

            lock (_syncLock)
            {
                _dict[key] = GetCount(key) + 1;
            }
        }
    }
}