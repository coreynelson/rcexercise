﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FaultPatternWebApp.Startup))]
namespace FaultPatternWebApp
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
