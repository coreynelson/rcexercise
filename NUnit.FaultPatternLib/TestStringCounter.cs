﻿using NUnit.Framework;
using RC.CodingChallenge.FaultPatternLib;

namespace RC.CodingChallenge.NUnit.FaultPatternLib
{
    [TestFixture]
    public class TestStringCounter
    {
        private string _a = "a";
        private string _b = "b";

        [Test]
        public void TestCount1()
        {
            // Arrange
            StringCounter counter = new StringCounter();

            // Act
            counter.IncrementCount(_a);

            // Assert
            Assert.AreEqual(1, counter.GetCount(_a));
            Assert.AreEqual(0, counter.GetCount(_b));
        }

        [Test]
        public void TestCountMany()
        {
            // Arrange
            StringCounter counter = new StringCounter();

            // Act
            counter.IncrementCount(_a);
            counter.IncrementCount(_a);
            counter.IncrementCount(_b);
            counter.IncrementCount(_a);
            counter.IncrementCount(_b);

            // Assert
            Assert.AreEqual(3, counter.GetCount(_a));
            Assert.AreEqual(2, counter.GetCount(_b));
        }
    }
}