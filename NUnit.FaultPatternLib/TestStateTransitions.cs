﻿using NUnit.Framework;
using RC.CodingChallenge.FaultPatternLib;
using System;

namespace RC.CodingChallenge.NUnit.FaultPatternLib
{
    [TestFixture]
    public class TestStateTransitions
    {
        private readonly string _id = "id";
        private readonly DateTimeOffset _now = DateTimeOffset.Now;
        private readonly DateTimeOffset _4min = DateTimeOffset.Now.Add(TimeSpan.FromMinutes(4));
        private readonly DateTimeOffset _6min = DateTimeOffset.Now.Add(TimeSpan.FromMinutes(6));

        [Test]
        public void TestDefaultToDefault()
        {
            // We could use a mocking framework here to mock out an IStateMachine but prepping an
            // Hvac is very simple so there is no need.
            IStateMachine machine = new Hvac(_id);
            TestStateTransition(
                machine,
                new DefaultState(machine),
                new StageUpdate(_4min, Stage.One),
                typeof(DefaultState),
                false);
        }

        [Test]
        public void TestDefaultToWait3()
        {
            IStateMachine machine = new Hvac(_id);
            TestStateTransition(
                machine,
                new DefaultState(machine),
                new StageUpdate(_4min, Stage.Three),
                typeof(Wait3State),
                false);
        }

        [Test]
        public void TestWait3ToDefaultLessThan5()
        {
            IStateMachine machine = new Hvac(_id);
            TestStateTransition(
                machine,
                new Wait3State(machine, _now),
                new StageUpdate(_4min, Stage.Two),
                typeof(DefaultState),
                false);
        }

        [Test]
        public void TestWait3ToDefault()
        {
            IStateMachine machine = new Hvac(_id);
            TestStateTransition(
                machine,
                new Wait3State(machine, _now),
                new StageUpdate(_4min, Stage.Zero),
                typeof(DefaultState),
                false);
        }

        [Test]
        public void TestWait3ToCycle23()
        {
            IStateMachine machine = new Hvac(_id);
            TestStateTransition(
                machine,
                new Wait3State(machine, _now),
                new StageUpdate(_6min, Stage.Two),
                typeof(Cycle23State),
                false);
        }

        [Test]
        public void TestCycle23ToDefaultZero()
        {
            IStateMachine machine = new Hvac(_id);
            TestStateTransition(
                machine,
                new Cycle23State(machine),
                new StageUpdate(_4min, Stage.Zero),
                typeof(DefaultState),
                true);
        }

        [Test]
        public void TestCycle23ToDefaultOne()
        {
            IStateMachine machine = new Hvac(_id);
            TestStateTransition(
                machine,
                new Cycle23State(machine),
                new StageUpdate(_4min, Stage.One),
                typeof(DefaultState),
                false);
        }

        [Test]
        public void TestCycle23ToCycle23()
        {
            IStateMachine machine = new Hvac(_id);
            TestStateTransition(
                machine,
                new Cycle23State(machine),
                new StageUpdate(_4min, Stage.Two),
                typeof(Cycle23State),
                false);
        }

        private void TestStateTransition(
            IStateMachine machine, 
            State startState, 
            StageUpdate update, 
            Type expectedEndStateType,
            bool expectedFault)
        {
            // Arrange
            machine.State = startState;

            // Act
            bool fault = machine.UpdateStage(update);

            // Assert
            Assert.IsInstanceOf(expectedEndStateType, machine.State);
            Assert.AreEqual(expectedFault, fault);
        }
    }
}