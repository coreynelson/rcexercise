﻿using NUnit.Framework;
using RC.CodingChallenge.FaultPatternLib;
using System.IO;

namespace RC.CodingChallenge.NUnit.FaultPatternLib
{
    [TestFixture]
    public class TestHvacFaultCounter
    {
        private string _device1 = "device1";
        ////private string _device2 = "device2";

        [Test]
        public void TestEmptyLog()
        {
            // Arrange
            string eventLog = string.Empty;
            IEventCounter counter = new HvacFaultCounter();
            StreamReader reader = GetStreamReader(eventLog);

            // Act
            using (reader)
            {
                counter.ParseEvents(_device1, reader);
            }
            
            // Assert
            Assert.AreEqual(0, counter.GetEventCount(_device1));
        }

        [Test]
        public void TestInvalidEventLog()
        {
            // Arrange
            string eventLog = "This is and invalid event log.";
            IEventCounter counter = new HvacFaultCounter();
            StreamReader reader = GetStreamReader(eventLog);

            // Act/Assert
            Assert.Throws<InvalidDataException>(delegate
            {
                using (reader)
                {
                    counter.ParseEvents(_device1, reader);
                }
            });
        }

        [Test]
        public void TestCount0()
        {
            // Arrange
            string eventLog =
@"2011-03-07 06:25:32	2
2011-03-07 09:15:55	3
2011-03-07 12:00:00	3
2011-03-07 12:03:27	2
2011-03-07 20:23:01	1";

            IEventCounter counter = new HvacFaultCounter();
            StreamReader reader = GetStreamReader(eventLog);

            // Act
            counter.ParseEvents(_device1, reader);

            // Assert
            Assert.AreEqual(0, counter.GetEventCount(_device1));
        }

        [Test]
        public void TestCount1NoCycle()
        {
            // Arrange
            string eventLog =
@"2011-03-07 06:25:32	2
2011-03-07 09:15:55	3
2011-03-07 12:00:00	3
2011-03-07 12:03:27	2
2011-03-07 20:23:01	0";

            IEventCounter counter = new HvacFaultCounter();
            StreamReader reader = GetStreamReader(eventLog);

            // Act
            counter.ParseEvents(_device1, reader);

            // Assert
            Assert.AreEqual(1, counter.GetEventCount(_device1));
        }

        [Test]
        public void TestCount1Cycling()
        {
            // Arrange
            string eventLog =
@"2011-03-07 06:25:32	2
2011-03-07 09:15:55	3
2011-03-07 12:00:00	3
2011-03-07 12:03:27	2
2011-03-08 09:15:55	3
2011-03-08 12:03:27	2
2011-03-08 12:03:28	2
2011-03-09 09:15:55	3
2011-03-09 12:00:00	3
2011-03-09 12:03:27	2
2011-03-09 20:23:01	0";

            IEventCounter counter = new HvacFaultCounter();
            StreamReader reader = GetStreamReader(eventLog);

            // Act
            counter.ParseEvents(_device1, reader);

            // Assert
            Assert.AreEqual(1, counter.GetEventCount(_device1));
        }

        [Test]
        public void TestCount2()
        {
            // Arrange
            string eventLog =
@"2011-03-07 06:25:32	2
2011-03-07 09:15:55	3
2011-03-07 12:00:00	3
2011-03-07 12:03:27	2
2011-03-07 20:23:01	0
2011-03-08 06:25:32	2
2011-03-08 09:15:55	3
2011-03-08 12:00:00	3
2011-03-08 12:03:27	2
2011-03-08 20:23:01	0";

            IEventCounter counter = new HvacFaultCounter();
            StreamReader reader = GetStreamReader(eventLog);

            // Act
            counter.ParseEvents(_device1, reader);

            // Assert
            Assert.AreEqual(2, counter.GetEventCount(_device1));
        }

        private StreamReader GetStreamReader(string log)
        {
            MemoryStream memStream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(log));
            return new StreamReader(memStream);
        }
    }
}
