﻿using NUnit.Framework;
using RC.CodingChallenge.FaultPatternLib;

namespace RC.CodingChallenge.NUnit.FaultPatternLib
{
    [TestFixture]
    public class TestHvac
    {
        [Test]
        public void TestDeviceId()
        {
            // Arrange
            string deviceId = "testId";
            Hvac hvac = new Hvac(deviceId);

            // Act / Assert
            Assert.AreEqual(deviceId, hvac.DeviceId);
        }
    }
}